# Instalación pre-commit #

Permite habilitar hook check-added-large-files que filtra archivos pesados (5000KB) en commits de repositorio local.
Permite habilitar hook check-bteqs que chequea buenas practicas DataOps en commits de repositorio local.

### Instalación ###

1. Requisito previo: Instalar Python 
     - No habilitar instalación para todos los usuarios
     - Habilitar Add Python to PATH, agrega python a variables de entorno 
2. Abrir cmd y ejecutar: pip install pre-commit
3. Con pull descargar archivo .pre-commit-config.yaml en repositorio local
4. En consola GIT de repositorio local ejecutar: pre-commit install  
5. Comprobación de instalación: git commit --allow-empty -m 'Hello world!'


### Who do I talk to? ###

* DataOps team

### Documentación ###

* https://pre-commit.com