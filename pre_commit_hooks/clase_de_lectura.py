#######################################################
# Primera metodos de queries
# Autor: BGOMEZM, RBURGOO
# Fecha: 20210520
#######################################################
import re
#import query_formatops_v1

class global_cleaning:

    def __init__(self,path_archivo):
        self.path_archivo = path_archivo

    def leer_sql(self):
        sql_file = open(self.path_archivo, 'r')
        sql_as_string = sql_file.read() # Lee el archivo dejandolo como string
        self.sql_as_string = sql_as_string.lower() # Minimizar caracteres
        return(self.sql_as_string)

    def sql_to_queries(self):
        queries = self.sql_as_string.split(';') # Particionamos la query
        lista_queries = []
        for query in queries:
            query_limpia = self.simple_cleaning(query) # Limpiamos la query
            metadata_query = self.break_in_pieces(query_limpia) # Incoporamos metadata de la query
            lista_queries.append({'query' : query,
                                 'query_limpia' : query_limpia,
                                 'metadata' : metadata_query}) # Almacenamos info                        
        return(lista_queries)

    def simple_cleaning(self,arm):
        temp = arm
        replacements = [
            (r"^\n+", "", 're.M')
            , (r"\n+$", "", 're.M')
            , (r"^\t+", "", 're.M')
            , (r"\t+$", "", 're.M')
            , (r"^\s+", "", 're.M')
            , (r"\s+$", "", 're.M')
            , (r',\n+|,\t+|,\s+', ',', 're.M')
            , (r"--.*", "", 're.M')
            , (r'/\*.*\*/', '', 're.M')
            , (r'/\*.*\*/', '', 're.S')
        ]
        for pat, repl, x in replacements:
            temp = re.sub(pat, repl, temp, flags=eval(x))
        temp = temp.lower().lstrip().rstrip()
        #temp = re.sub(r'\s+', ' ', temp, flags=re.M)
        #temp = re.sub(r'[\n+\t+]',' ',temp)
        return temp

    def break_in_pieces(self,cuerpo):
        analisis = cuerpo

        if bool(re.findall(r'\s*\t*\n*create',analisis.lower())):
            dic = dict(zip(['create','select'],analisis.split('(',1)))

            #check si es que existe la tabla
            tabla_output = re.search(r'(table(.*)\s+as)|(table(.*),fallback)|(table(.*))',dic['create'],flags=re.S).groups()
            tabla_output =[x for x in tabla_output if x!=None][-1].strip()
            query_output = dic['select']#simple_cleaning(dic['select'])

            dic2 = dict(zip(['tipo','tabla', 'select'],['create',tabla_output,query_output]))

        elif bool(re.findall(r'\s*\t*\n*\.if errorcode',analisis.lower())):
            dic = dict(zip(['if_errorcode','exit_num'],analisis.split('then',1)))
            dic2 = dict(zip(['tipo', 'tabla', 'select'], ['if_errorcode', None, dic]))

        elif bool(re.findall(r'\s*\t*\n*collect (stats|statistics)', analisis.lower())):
            dic = dict(zip(['collect_stats', 'tabla'], analisis.split('on', 1)))
            try:
                select = dic['collect_stats']
                index = re.search(r'(index.*)(,[\s\n\t]*)(column.*)',select,flags=re.S).groups()[0].strip()
                columns = re.search(r'(index.*)(,[\s\n\t]*)(column.*)',select,flags=re.S).groups()[2].strip()

                comp = re.compile(r'(index[\s\t\n]+)|(column[\s\t\n]+)')
                index_values = comp.split(index)[-1]
                column_values = comp.split(columns)[-1]
                desglose_collect = dict(zip(['index', 'column'], [index_values,column_values]))

            except AttributeError:
                desglose_collect = dict(zip(['index', 'column'], [None, None]))

            dic2 = dict(zip(['tipo', 'tabla', 'select'], ['collect', dic['tabla'].strip(),desglose_collect]))

        elif bool(re.findall(r'\s*\t*\n*insert',analisis.lower())):

            parte_a = re.search(r'\s*\n*\t*insert\s*\n*\t*into\s*\n*\t*(.*)\s*\n*\t*(select|sel)',analisis,flags = re.S).group(1).strip()
            parte_b = re.search(r'select.*',analisis,flags = re.S).group().strip()

            #parte_a_clean = simple_cleaning(parte_a)
            #parte_b_clean = simple_cleaning(parte_b)

            lista = [parte_a,
                     parte_b]

            dic = dict(zip(['insert','select'],lista))
            #check si es que existe la tabla
            dic2 = dict(zip(['tipo','tabla','select'],['insert',dic['insert'],dic['select']]))

        elif bool(re.findall('\s*\t*\n*drop', analisis.lower())):
            parte_a = re.search(r'\s*\n*\t*drop\s*\n*\t*table\s*\n*\t*(.*)\s*\n*\t*', analisis,flags=re.S).group(1).strip()
            parte_b = re.findall(r'(?:.*)(drop)(?:.*)', analisis,flags=re.S)#.group(1).strip()

            lista = [parte_a,
                     parte_b[0]]

            dic = dict(zip(['table', 'drop'], lista))
            # check si es que existe la tabla
            dic2 = dict(zip(['tipo', 'tabla', 'select'], ['drop', dic['table'], analisis]))

        else:
            dic2 = dict(zip(['tipo','tabla', 'select'],[None,None,None]))
        return dic2

#class chequeos_query(global_cleaning):

#    def __init__(self,argumento):


#######################################################
# Pruebas (eliminar posteriormente)
#######################################################
#objeto = global_cleaning('C:/Users/hpachec/Desktop/Chequeo_bteq/Repositorio/analytics_personas/funciones_compartidas/QueryOps/query_ejemplo.sql')

#a = objeto.leer_sql()
#b = objeto.sql_to_queries()
#Detectar uso de DB BCIMKT
#resultado = query_formatops_v1.check_db_bcimkt(a)
#print('Posiciones de uso de DB BCIMKT: ', resultado)

#Detectar uso de CREATE TABLE AS
#resultado = query_formatops_v1.check_create_table_as(a)
#print('Usos de CREATE TABLE AS: ', resultado)
