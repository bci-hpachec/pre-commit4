import sys
sys.path.append("./Clases")
from pre_commit_hooks.clase_de_lectura import global_cleaning
from pre_commit_hooks.clase_de_chequeo import checks
import re


path = './Queries/ejemplo2.sql'

#objeto = global_cleaning(path)
#objeto.leer_sql()
#b = objeto.sql_to_queries()
#objeto2 = checks(b)
#metodos_listados = [x for x in objeto2.__dir__() if not x.startswith('_')]
metodos_dic = ({'tipo': {'create':{'prelacion':['check_create_table_as',
                                                'check_query_anidada',
                                                'check_select_all',
                                                'check_prefijo_tabla',
                                                'check_prefijo_campo',
                                                 'check_prefijo_join',
                                                'check_formato_airflow',
                                                'check_errorcode']} #'check_db_bcimkt'
                         ,'insert':{'prelacion':['check_query_anidada',
                                                 'check_par_create_insert',
                                                 'check_select_all',
                                                 'check_groupby',
                                                 'check_fechas',
                                                 'check_db_bcimkt',
                                                 'check_or',
                                                 'check_prefijo_tabla',
                                                 'check_prefijo_campo',
                                                 'check_prefijo_join',
                                                 'check_currentdate',
                                                 'check_fechas_duras',
                                                 'check_formato_airflow',
                                                 'check_likes',
                                                 'check_alias',
                                                 'check_query_comentada',
                                                 'check_errorcode']}
                         ,'drop': {'prelacion':['check_db_bcimkt',
                                                'check_prefijo_tabla',
                                                'check_formato_airflow']}
                         ,'collect':{'prelacion':['check_errorcode',
                                                  'check_db_bcimkt',
                                                  'check_prefijo_tabla',
                                                  'check_prefijo_campo',
                                                  'check_formato_airflow']}
                         }
                })  # Almacenamos info

recorrido_full = {'tipo':['check_collect',
                  'check_quit',
                  'check_inicio_fin',
                  'check_encabezado',
                  'check_tbls_encabezado',
                  'check_drop_finales']}

#lista = [*metodos_dic['tipo'].keys()] #unpack keys

#objeto = global_cleaning(path)
#objeto.leer_sql()
#arg = objeto.sql_to_queries()
#objeto2 = checks(arg)

class orquestador_check_query(checks):

    def __init__(self,path):
        self.objeto = global_cleaning(path)
        self.objeto.leer_sql()
        self.arg = self.objeto.sql_to_queries()
        self.objeto2 = checks(self.arg)

    def let_it_rip(self):
        dic_total = []
        for item in self.arg:
            try:
                tipo = item['metadata']['tipo']
                #print('------------------------------------------------------\n',tipo) #PARA DEBUGEAR
                foodict = {k: v for k, v in metodos_dic['tipo'].items() if k == tipo}
                lazy_method = [x for x in foodict[tipo]['prelacion']]
                output = []
                for x in lazy_method:
                    try:
                        obj = self.__getattribute__('{}'.format(x))(item)
                        #print('--------->\n\t\t\t', x)#PARA DEBUGEAR
                        #print(obj)#PARA DEBUGEAR
                        output.append(obj)
                    except:
                        #print('error al ejecutar metodo {x} en {tipo}'.format(x=x,tipo=tipo))
                        obj = {'error':' BUG','evidencia':[]}
                        output.append(obj)
                #print('output ok y encontrado: ', output)#PARA DEBUGEAR
                booleano = [x['error'] for x in output]
                evidencia = [x['evidencia'] for x in output]
                dic = dict(zip(['objeto_query','metodo','error', 'evidencia'], [item,lazy_method, booleano,evidencia]))
                dic_total.append(dic)
            except KeyError as e:
                pass
                #print('agregar tipo {tipo}!'.format(tipo=item['metadata']['tipo']))#PARA DEBUGEAR

        output_full_recorrido = []
        for x in recorrido_full['tipo']:
            try:
                obj = self.__getattribute__('{}'.format(x))()
                obj['metodo'] = x
                output_full_recorrido.append(obj)
            except:
                #print('error al ejecutar metodo full recorrido en {tipo}'.format(tipo=x))
                obj = {'error': ' BUG', 'evidencia': [],'metodo':x}
                output_full_recorrido.append(obj)

        return (dic_total,output_full_recorrido)

#instancia = orquestador_check_query(path)
#a = instancia.let_it_rip()


#import pickle
#def save_object(obj, filename):
#    with open(filename, 'wb') as outp:  # Overwrites any existing file.
#        pickle.dump(obj, outp, pickle.HIGHEST_PROTOCOL)

# sample usage
#save_object(dic, 'dic_ejemplo.pkl')

#import pickle

#filehandler = open("dic.pkl", 'wb')
#pickle.dump(dic, filehandler)

#diccionario = a[1]['objeto_query']