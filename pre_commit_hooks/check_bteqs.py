"""
-- CHEQUEO BTEQs con buenas practicas DataOps-
"""

from pre_commit_hooks.orquestador_check_query import orquestador_check_query 
from pre_commit_hooks.generador_log import generador_log_bteq

from typing import Optional
from typing import Sequence
import argparse
#from pre_commit_hooks.util import staged_files


def check_bteqs(
        filenames: Sequence[str]
) -> int:
    # Find all staged files that are also in the list of files pre-commit tells
    # us about
    retv = 0
    filenames_filtered = set(filenames)
    #filenames_filtered &= staged_files()

    for path_query in filenames_filtered:
        if '.sql' in path_query:
            rslt_orq = orquestador_check_query(path_query)
            sql_evaluado = rslt_orq.let_it_rip()
            generador_log_bteq(sql_evaluado, path_query)

    return retv

def main(argv: Optional[Sequence[str]] = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'filenames', nargs='*',
        help='Filenames pre-commit believes are changed.',
    )
    args = parser.parse_args(argv)
    
    return check_bteqs(
        args.filenames
    )


if __name__ == '__main__':
    raise SystemExit(main())