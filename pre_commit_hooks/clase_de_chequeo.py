#from query_ops_core import global_cleaning
import sys
sys.path.append("./Clases")
from pre_commit_hooks.clase_de_lectura import global_cleaning
from dateutil.parser import parse
import re
from itertools import compress
from collections import Counter
from pre_commit_hooks.query_formatops_v1 import *

class checks(global_cleaning):

    def __init__(self,list_sts):
        self.arg = list_sts

    def find_subquery(self,diccionario):
        temp = diccionario['metadata']['select']
        sepa2 = re.compile(r'left join|inner join|full join|with data', flags=re.S)
        a = sepa2.split(temp.lower())
        a = [re.sub(r'\s+|\t+|\n+',r' ',x) for x in a]
        b = []
        for x in a:
            try:
                b.append(''.join(re.findall(r'(\(\s*select)(.*)\s*\)', x, flags=re.S)[0]))
            except:
                pass

            sepa2 = re.compile(r'\)(.*)\s+on\s+', flags=re.S)
            c = [sepa2.split(x)[0] for x in b]
            c = [re.sub(r'^\(\s*\n*\t*','',x) for x in c]
            #c = [re.search(r'\(.*\)', x, flags=re.S).group() for x in b]
            #re.sub(r'')c[1]

        return c

    def check_query_anidada(self,diccionario):

        res = self.find_subquery(diccionario)
        return dict(zip(['error','evidencia'],[(res !=[]),res]))

    def check_par_create_insert(self,diccionario):
        """si hay un create, debe existir un insert"""
        table_name = diccionario['metadata']['tabla']
        bool_list = []
        try:
            for x in self.arg:
                t = x['metadata']
                check = t['tabla']
                tipo = t['tipo']
                bool_list.append(table_name==check and tipo=='insert')

            index = [i for i, x in enumerate(bool_list) if x][0]
            query_match = self.arg[index]

        except:
            bool_list = False
            query_match = False
            pass

        dic =dict(zip(['error','evidencia'],[bool(bool_list*-1),query_match]))
        return dic#bool,query_match

    def check_select_all(self,diccionario):
        """
        Función que revisa si existe * en la query
        Autor : Ramiro Burgos
        """
        analisis = diccionario['metadata']['select']
        try:
            #evidencia = re.findall(r'\*', analisis, flags=re.S)
            evidencia = re.findall(r'([a-z].*\.\*)|(select\s*\n*\t*\*)', analisis, flags=re.S)
            lista = [next(x for x in t if x) for t in evidencia]

            lista2 = [re.search(r'select\s*\t*\n*(.*)\s*\t*\n*',x).group(1) for x in lista]
            #lista = [(x.split('.*')[0],x) for x in lista if x!=None]
            dict2 = dict(zip(['error','evidencia'],[bool(lista),lista2]))
        except:
            dict2 = dict(zip(['ref', 'evidencia'], [False, False]))
        return dict2

    def as_between_join(self,diccionario):

        sepa2 = re.compile(r'\s*\t*\n*from\s*\t*\n*(?:\()|left|inner|full', flags=re.S)

        a = sepa2.split(diccionario['metadata']['select'].lower().lstrip().rstrip())

        prev = [re.sub(r'(.*(join|from)\s*\n*\t*)', '', x.strip().split('on')[0]) for x in a]

        result = [re.sub(r'(\s)+(as)*(\s)*', r' as ', x.strip()) for x in prev]

        return result

    def check_groupby(self, diccionario):
        """
           Revisa si despues de un group by aparecen los campos o una numeracion
           -- Requisito de funcionamiento
               No pueden existir anidadas
        """
        query_limpia = diccionario['query_limpia']

        #Primero chequeamos si hay un group by
        pos_groupby = query_limpia.find("group by")
        #Vemos le largo del string (usualmente el group by va al final)
        # -- pendiente: mejorar caso en que no vaya al final
        # -- probablemente igual cumple
        pos_end = len(query_limpia)

        if pos_groupby > -1: # Esto es si encuentra un group by en la query
            section_groupby = query_limpia[pos_groupby:pos_end] # Seleccionamos evidencia
            result = any(map(str.isdigit, section_groupby)) # True si encuentra un numero
            # Generamos resultado final
            if result == True:
                dic =dict(zip(['error','evidencia'],[result,section_groupby]))
            else:
                dic =dict(zip(['error','evidencia'],[result,section_groupby]))
        else: # Este caso ocurre cuando no existe un group by en la query
            dic =dict(zip(['error','evidencia'],[False,"no existe group by"]))

        return dic

    def is_date(self, string, fuzzy=False):
        """
        Return whether the string can be interpreted as a date.

        :param string: str, string to check for date
        :param fuzzy: bool, ignore unknown tokens in string if True
        """
        try:
            parse(string, fuzzy=fuzzy)
            return True

        except ValueError:
            return False

    def check_fechas(self, diccionario):
        """
           Revisa si existe fecha en duro o algun current date
        """

        query_limpia = diccionario['query_limpia']

        find_current_date = query_limpia.find("current_date")
        # hacemos un split por espacio
        elementos_query = query_limpia.split(" ")
        numeros =  [x for x in elementos_query if any(c.isdigit() for c in x)]
        numeros =  [x for x in numeros if any(len(c) >= 5 for c in x)]
        result = any(map(self.is_date, numeros))
        evidencia = numeros

        if result == True or find_current_date > 0:
            result = True
            evidencia.append("current_date")

        dic =dict(zip(['error','evidencia'],[result,evidencia]))

        return dic

    def check_db_bcimkt(self,diccionario):
        """
           Funcion para encontrar uso de DB BCIMKT, será eliminada
           Autor : Hugo Pacheco
        """

        query_limpia = diccionario['query_limpia']
        reg_exp = r'bcimkt\..+'
        result = re.findall(reg_exp, query_limpia)
        dic = dict(zip(['error', 'evidencia'],[bool(result), result]))
        return dic

    def check_create_table_as(self,diccionario):
        """
           Funcion para encontrar uso de CREATE TABLE AS
           Autor : Hugo Pacheco
        """
        query_limpia = diccionario['query_limpia']
        reg_exp = r'create.+table.+\sas'
        result = re.findall(reg_exp, query_limpia)
        dic = dict(zip(['error', 'evidencia'],[bool(result), result]))
        return dic

    def check_or(self,diccionario):
        """
           Funcion para encontrar uso de OR dentro de un WHERE
           Autor : Hugo Pacheco
        """
        query_limpia = diccionario['query_limpia']
        reg_exp = r'where\s.+\sor\s.+'
        result = re.findall(reg_exp, query_limpia)
        dic = dict(zip(['error', 'evidencia'],[bool(result), result]))
        return dic

    def check_collect(self):
        """
           Funcion que verifica uso de COLLECT para estadisticas
           Autor : Hugo Pacheco
        """
        lista = []
        tabla = []
        check_insert = 0
        for index, i in enumerate(self.arg):
            if i['metadata']['tipo'] == 'insert':
                check_insert = 1
                if self.arg[index+2]['metadata']['tipo'] == 'collect':
                    result = self.arg[index+2]['query_limpia']
                    tabla_collect = self.arg[index+2]['metadata']['tabla']
                    for t, j in zip(tabla, lista):
                        if i['metadata']['tabla'] == t and j == [] and t == tabla_collect:
                            lista[lista.index(j)] = t
                elif self.arg[index+1]['metadata']['tipo'] == 'collect':
                    result = self.arg[index+1]['query_limpia']
                    tabla_collect = self.arg[index+1]['metadata']['tabla']
                    for t, j in zip(tabla, lista):
                        if i['metadata']['tabla'] == t and j == [] and t == tabla_collect:
                            lista[lista.index(j)] = t
                else:
                    result = []
                lista.append(result)
                tabla.append(i['metadata']['tabla'])
        if check_insert == 0:
            lista.append("Query sin insert")
        dic = dict(zip(['error', 'evidencia'],[[] in lista, lista]))
        return dic

    def check_quit(self):
        """
           Funcion que verifica uso de QUIT 0 para constatar proceso finalizado
           Autor : Hugo Pacheco
        """
        query_limpia = self.arg[-2]['query_limpia']
        reg_exp = r'\.quit 0'
        result = re.findall(reg_exp, query_limpia)
        dic = dict(zip(['error', 'evidencia'],[not bool(result), result]))
        return dic

    def check_inicio_fin(self):
        """
           Funcion que verifica uso de UTF8 y registro de tiempo de inicio y fin
           Autor : Hugo Pacheco
        """
        query_limpia = self.arg[0]['query_limpia']
        reg_exp = r"\.set session charset 'utf8'"
        query_limpia2 = self.arg[1]['query_limpia']
        query_limpia3 = self.arg[-3]['query_limpia']
        reg_exp2 = r'select date,\s*time'
        result = re.findall(reg_exp, query_limpia)
        result2 = re.findall(reg_exp2, query_limpia2)
        result3 = re.findall(reg_exp2, query_limpia3)
        lista = [result, result2, result3]
        dic = dict(zip(['error', 'evidencia'],[[] in lista, lista]))
        return dic

    def check_prefijo_tabla(self,diccionario):
        """
        Revisa que prefijo en tablas contenga esquema OPS; en caso de encontrar error se marca True
        y la evidencia de las tablas que no cumplen
        Autor : Ramiro Burgos
        """
        analisis = diccionario["metadata"]["tabla"] #crea temporal de string/tabla_name de trabajo
        try:
            pattern = re.search(r'\..+',analisis).group()[1:] #busca patron al separar . en el nombre de la tabla
            prefix = re.search(r'.*?\_', pattern).group() # busca patron en _ para extraer prefijos (si es que hubiese)
        except AttributeError:
            pattern = None #condicion de salida
            prefix = None #condicion de salida

        dict2 = dict(zip(['error', 'evidencia'], [prefix not in ['s_','p_','c_','t_','r_','i_'], pattern]))

        return dict2

    def extract_variables(self,string):
        """
        Función auxiliar que permite extraer los campos/variables de salida de la query
        Autor : Ramiro Burgos
        """
        sepa2 = re.compile(r'select|from', flags=re.S) #expresión regular compilada para utilizar como forma de split
        try:
            separacion = sepa2.split(string.lstrip().rstrip())
            if len(separacion)>1:
                separacion = separacion[1]
            else:
                separacion = separacion
            separacion_col = separacion.split(',') #separacion de los campos por ,
            lista_variables = [re.search(r'\s*([\S]+)$',col_string.rstrip().lstrip(),flags=re.M).group(1) for col_string in separacion_col] #extrae lista de variables
            lista_variables_clean = [x.split('.')[-1] for x in lista_variables] #limpia campos con referencias, es decir, a.column_name en column_name
            dict2 = dict(zip(['variables', 'variables_clean'], [lista_variables,lista_variables_clean])) #variables: variables con prefixo de tabla ; variables_clean: variables sin prefijo de tabla

        except AttributeError:
            dict2 = dict(zip(['variables', 'variables_clean'], [None,None])) #condicion de salida
            pass

        return dict2

    def check_prefijo_campo(self,diccionario):
        """
        Función de chequeo para identificar si campos/variables cumplen con prefijo OPS
        error es True cuando se encuentra incidencia; evidencia: lista de variables con formato no acorde a estándar OPS
        Autor : Ramiro Burgos
        """
        try:
            string = diccionario["metadata"]["select"] #se carga string query de trabajo
            diccionario_variables = self.extract_variables(string) #utiliza función aux para extraer nombre de los campos
            lista_variables = diccionario_variables['variables_clean'] #lista de variables sin las referencias de tabla
            prefix_list = [x[:1] not in ['s','p','c','t','r','i'] or x[1:3] not in ['e_','d_','f_','c_'] for x in lista_variables]
            dict2 = dict(zip(['error', 'evidencia'], [any(prefix_list), list(compress(lista_variables, prefix_list))]))
        except TypeError:
            dict2 = dict(zip(['error', 'evidencia'], [False, None]))
        return dict2

    def check_prefijo_join(self,diccionario):
        """
        Función de chequeo  para identificar tablas y referencias en caso que las hubiese
        (por ejemplo bcimkt.table1 as A -> bcimkt.table1;A)
        Autor : Ramiro Burgos
        """
        try:
            query = diccionario['metadata']['select'] #se carga string/query de trabajo
            sepa2 = re.compile(r'\s+from\s+(?:!\()|left join|inner join|full join', flags=re.M) #expresión regular compilada para luego hacer split en query
            a = sepa2.split(query) #split inicial de query para descomponer query en referencias y tablas

            # limpieza de string para aquellos casos donde la primera query es subquery
            # (eg: select a,b,e from \(select... trafo a : (select...
            prev = [re.sub(r'.*?\(\s*select','(select',x,flags=re.S) for x in a if x != None]

            # all lo que venga en las uniones luego de on quedan en vacio
            prev2 = [re.sub(r'\s+(on)+.*','',x,flags=re.S) for x in prev]

            # repaso  de limpieza (elimina all hacia atras de from) para queries simples sin subqueries ni uniones
            prev2 = [re.sub(r'.*from\s*','',x,flags=re.S) if x.strip().startswith('select') else x for x in prev2]

            sepa2 = re.compile(r'\s+(as)+\s+', flags=re.M)
            prev_clean = [sepa2.split(x) for x in prev2] #si contiene as en union se elimina

            # limpieza para obtener tabla_name,prefijo/referencia
            result = [ (x[0],x[-1]) if len(x)>1 else x[-1].strip().split() for x in prev_clean]

            tablas = [x[0] for x in result]

            # si el len(x) es >2 es porque es una query simple sin uniones ni subqueries // además no referenciada
            prefijos = [x[-1].strip() if len(x)<=2 else None for x in result]
            dict2 = dict(zip(['tabla_items','prefijo'], [tablas,prefijos]))

        except:
            dict2 = dict(zip(['tabla_items','prefijo'], [None,None]))

        dict3 = dict(zip(['error', 'evidencia'], [bool(dict2['prefijo']*-1), dict2]))

        return dict3

    def check_groupby(self, diccionario):
        """
        Funcion que eevisa si despues de un group by aparecen los campos o una numeracion
        -- Requisito de funcionamiento: No pueden existir anidadas
        Autor : Bruno Gomez
        """
        query_limpia = diccionario['query_limpia']

        #Primero chequeamos si hay un group by
        pos_groupby = query_limpia.find("group by")
        #Buscamos el largo de la query
        pos_end = len(query_limpia)

        # Ahora definimos hasta donde llega el group by
        # -- Agregamos aca tipicos comandos de SQL que se usan post group by para posteriormete seleccionar solo
        # -- la parte del group by
        comandos = ["order by","qualify","where"] # agregar casos si faltan

        # Esto es si encuentra un group by en la query
        if pos_groupby > -1:

            # Dejamos solamente la informacion del group by
            section_groupby = query_limpia[pos_groupby:pos_end] # Seleccionamos evidencia
            indice = pos_end
            for i in comandos:
                revisar = section_groupby.find(i)
                if (revisar != -1) and (revisar < indice):
                    indice = revisar
            section_groupby = section_groupby[0:indice] # Recortamos evidencia en caso que aplique

            # Revisamos si existe un caso de case when
            detectar_casewhen = query_limpia.find("case ")
            # Revisamos si existen numeros en la seccion del group by
            result = any(map(str.isdigit, section_groupby))

            # Generamos resultado final
            if result == True:
                # -- Identifica numeros en el codigo
                dic =dict(zip(['error','evidencia'],[result,section_groupby]))
            elif detectar_casewhen != -1 :
                # -- Cuando hay un case when lo marcamos como error, porque no es necesario y complejiza el codigo
                dic =dict(zip(['error','evidencia'],[True," observacion: hay un case when en el group by"]))
            else:
                # -- Existe un group by pero pasa los casos de observacion
                dic =dict(zip(['error','evidencia'],[result,section_groupby]))
        else: # Este caso ocurre cuando no existe un group by en la query
            dic =dict(zip(['error','evidencia'],[False,"no aplica chequeo"]))

        return dic

    def check_currentdate(self, diccionario):
        """
           Funcion: Revisa si existe un current date en el codigo
           Autor: Bruno Gomez
        """
        query_limpia = diccionario['query_limpia']
        find_current_date = query_limpia.find("current_date")

        if find_current_date > 0:
            result = True
            evidencia = "current_date"

        dic = dict(zip(['error','evidencia'],[result,evidencia]))

        return dic

    def check_fechas_duras(self, diccionario):
        """
           Funcion revisa si existe fecha en duro en la query
           Autor: Bruno Gomez
        """

        query_limpia = diccionario['query_limpia']

        # Buscamos diferentes formatos de fechas
        f_one = re.search(r'(\d+/\d+/\d+ )', query_limpia) # buscamos fechas formato YY/MM/DD | YYYY/MM/DD o sus combinaciones
        f_two = re.search(r'(\d+-\d+-\d+ )', query_limpia) # buscamos fechas formato YY-MM-DD | YYYY-MM-DD o sus combinaciones
        f_three = re.search(r'(\d{6} )', query_limpia) # -- fechas formato YYYYMM

        checks = [f_one, f_two, f_three]
        test = map(bool, checks) # revisamos si existe una coincidencia
        result = any(test) # Vemos si hay algun caso

        # eliminamos los casos de None
        non_none_checks = filter(None.__ne__, checks)
        list_of_values = list(non_none_checks)

        # Construimos la evidencia en los casos que hay match
        evidencia = []
        for i in list_of_values:
            evidencia.append(i.group(0))

        evidencia = ' ; '.join(evidencia)

        dic =dict(zip(['error','evidencia'],[result,evidencia]))

        return dic

    def check_indentacion(self,diccionario):
        """
        funcion que permite chequear formatops de indentacion
        Autor : Ramiro Burgos
        """
        query = diccionario['metadata']['select']  # se carga string/query de trabajo
        a = re.sub(r'([\s\n\t]+)(select|from|where|left|right|inner|qualify)',r'\n\2',query)
        b = re.sub(r'([\s\n\t]*)(,)',r'\n    \2',a)

        return b

    def check_formato_airflow(self,diccionario):
        """
        Funcion para reconocer si la tabla generada contiene formato de Airflow
        Aplica sobre metadata del tipo create e insert
        Autor: Ramiro Burgos
        """
        tipo = diccionario['metadata']['tipo']
        query = diccionario['metadata']['tabla']  # se carga string/query de trabajo con el nombre de la tabla creada o insertada
        try:
            salida = [re.search(r'({\s*next_ds_nodash\s*}|{\s*ds_nodash\s*})',query).group() for x in query if tipo in ['create','insert', 'drop', 'collect']]
        except AttributeError:
            salida = False

        dict2 = dict(zip(['error', 'evidencia'], [not bool(salida), query]))
        return dict2

    def check_likes(self,diccionario):
        """
        Funcion que permite identificar si existen elementos LIKE en la query
        Autor: Ramiro Burgos
        """
        query = diccionario['metadata']['select']  # se carga string/query de trabajo
        salida = re.findall(r'(?:like any|like)\s+\([a-zA-Z0-9%]+\)',query)
        dict2 = dict(zip(['error', 'evidencia'], [bool(salida), salida]))
        return dict2

    def check_alias(self,diccionario):
        """
        Funcion que permite identificar si los campos del select contienen prefijos de tablas
        Autor: Ramiro Burgos
        """
        query = diccionario['metadata']['select']  # se carga string/query de trabajo
        diccionario_variables = self.extract_variables(query) #utiliza función aux para extraer nombre de los campos
        salida = [x for x in zip(diccionario_variables['variables'],diccionario_variables['variables_clean']) if x[0]==x[1]]
        dict2 = dict(zip(['error', 'evidencia'], [bool(salida), salida]))
        return dict2

    def check_query_comentada(self,diccionario):
        """
        Función que identifica si query en análisis presenta comentarios
        Autor: Ramiro Burgos
        """
        query = diccionario["query"]
        find_elements = [
              (r"--.*", 're.M')
        #    , (r'/\*.*\*/', 're.M')
           , (r'/\*[a-zAz0-9\s]+\*/', 're.S')
        #   , (r'/\*.*\*/', 're.M')
        ]
        salida = []
        for pat, x in find_elements:
            temp = re.findall(pat, query, flags=eval(x))
            salida.append(temp)
        dict2 = dict(zip(['error', 'evidencia'], [bool(salida), salida]))
        return dict2

    ##################################################
    # -- Chequeos de codigo completo
    ##################################################
    def check_encabezado(self):
        """
            Revisa si existe un encabezado en la BTEQ
            Autor: Bruno Gomez
        """
        a = self.arg
        split_por_comentarios = a.split("*/")

        # dejamos solamente la parte interior de los comentarios /* interior */
        comentarios = []
        for i in split_por_comentarios:
            interior_comentario = i.find("/*")
            try:
                comentarios.append(i[interior_comentario:len(i)])
            except:
                pass # para el caso donde no hay comentarios

        # Los primeros dos comentarios siempre deben ser de Descriptivo activo y tablas usadas
        descriptivo = comentarios[0]
        tablas_usadas = comentarios[1]

        # Revisamos si el primero comentario tiene indicios de
        # descripcion, autor, fecha y empresa
        # ver como convertirlo a una solucion mas elegante
        tst1 = descriptivo.find('dscrpcn')
        tst2 = descriptivo.find('descripcion')
        tst3 = descriptivo.find('autor')
        tst4 = descriptivo.find('fecha')
        tst5 = descriptivo.find('empresa')

        # Revisamos si el segundo comentario tiene indicios de
        # tablas de entrada y salida
        test5 = tablas_usadas.find('entrada')
        test6 = tablas_usadas.find('salida')

        # Construimos resultados
        condicion_1 = ((tst1 != -1) or (tst2 != -1)) and (tst3 != -1) and (tst4 != -1) and (tst5 != -1) #Encabezado 1
        condicion_2 = (test5 != -1) and (test6 != -1)  #Encabezado 2

        if (condicion_1 == False) and (condicion_2 == False):
            dic =dict(zip(['error','evidencia'],[False,'No hay indicios de encabezado en formato OPS']))
        elif (condicion_1) == False:
            dic =dict(zip(['error','evidencia'],[False,'no se encuentra sección de descripcion, autor, fecha, empresa o esta incompleto']))
        elif (condicion_2) == False:
            dic =dict(zip(['error','evidencia'],[False,'no hay seccion de tablas de entrada o salida']))
        else:
            dic =dict(zip(['error','evidencia'],[True,'Ok']))

        return(dic)

    def check_tbls_encabezado(self):
        """
            Revisa si las tablas del encabezado existen
            Autor: Bruno Gomez
        """

        a = self.arg

        # Solucionamos problemas de espacio por fechas de airflo
        a = re.sub(r"{{ +", "{{", a)
        a = re.sub(r" +}}", "}}", a)

        split_por_comentarios = a.split("*/")

        # dejamos solamente la parte interior de los comentarios /* interior */
        comentarios = []
        for i in split_por_comentarios:
            interior_comentario = i.find("/*")
            try:
                comentarios.append(i[interior_comentario:len(i)])
            except:
                pass # para el caso donde no hay comentarios

        # Los primeros dos comentarios siempre deben ser de Descriptivo activo y tablas usadas
        tablas_usadas = comentarios[1] # aca tienen que estar los comentarios por formato

        # parte 1: recuperamos las tablas de entrada en encabezado
        split_string = tablas_usadas.split("entrada", 1)
        desp_entrada = split_string[1]
        split_string = desp_entrada.split("salida", 1)
        txt_entradas = split_string[0]

        tablas_entrada = all_db_in_sql(txt_entradas)

        # parte 2: recuperamos las tablas de salida en encabezado
        split_string = tablas_usadas.split("salida", 1)
        txt_salidas = split_string[1]

        tablas_salida = all_db_in_sql(txt_salidas)

        # parte 3: recuperamos tablas de entrada en codigo
        # pendiente eliminar encabezado
        tablas_entrada_sql = db_entrada(a, formato = "lista")

        # parte 4: recuperamos tablas de salida en codigo
        # pendiente eliminar encabezado
        tablas_salida_sql = db_salida(a, formato = "lista")

        # parte 5: filtramos las tablas de salida que puedan estar en las de entrada
        # esto pasa especialmente con las tablas de esquema productivo o desarollo
        tablas_entrada_sql = list(filter(lambda a: a not in tablas_salida_sql, tablas_entrada_sql))

        # Juntamos tablas de entrada de encabezado y codigo
        union_entradas = list(tablas_entrada) + tablas_entrada_sql
        # Juntamos tablas de salida de encabezado y codigo
        union_salidas = list(tablas_salida) + tablas_salida_sql

        # Revisamos coincidencias
        # Si en conteo una tabla aparece 1 vez es porque hay discrepancia
        count_entrada = dict(Counter(union_entradas))
        count_salida = dict(Counter(union_salidas))
        # -- dejamos los que aparecen 1 vez
        count_entrada_errores = [k for k,v in count_entrada.items() if v <= 1]
        count_salida_errores = [k for k,v in count_salida.items() if v <= 1]

        tablas_con_errores = count_entrada_errores + count_salida_errores

        if tablas_con_errores != []:
            dic =dict(zip(['error','evidencia'],[False,tablas_con_errores]))
        else:
            dic =dict(zip(['error','evidencia'],[True,'Ok']))

        return(dic)

    def check_drop_finales(self):
        """
            Revisa si se eliminan las tablas temporales en la BTEQ
            Autor: Bruno Gomez
        """
        a = self.arg

        # tengo una forma de reconocer todas las queries
        creates = []
        drops = []
        for i in range(0,len(b)) :
            posicion = i
            tipo_query = b[i]['metadata']['tipo']
            tabla_query = b[i]['metadata']['tabla']

            if tipo_query == 'create':
                # Almaceno posicion en que se crea la tabal y el nombre
                creates.append({'position' : i
                              ,'tabla' : tabla_query} )
            if (tipo_query == 'drop'):
                drops.append({'position' : i
                              ,'tabla' : tabla_query} )

        # camiamos levemente el formato de las listas
        # -- eventualmente se puede simplificar el codigo desde la parte anterior
        lista_tablas_drops = []
        lista_tablas_creates = []
        for i in drops:
            lista_tablas_drops.append(i['tabla'])
        for i in creates:
            lista_tablas_creates.append(i['tabla'])

        #Elimininamos las de precalculo
        lista_tablas_drops = [x for x in lista_tablas_drops if not x.startswith("edw_tempusu.p")]

        #Contamos numero de apariciones
        from collections import Counter
        resultados = Counter(lista_tablas_drops)

        # hago una interseccion con las tablas que se crean
        tbls_bajo_corte = {x: count for x, count in resultados.items() if count <= 1}
        # -- si sale una vez puede que (1) no tenga un drop final (2) puede que ya no exista en el codigo
        tbls_sobre_corte = {x: count for x, count in resultados.items() if count >= 3}
        # -- si sale mas de 3 veces es probable que se encuentre duplicado o haya un error de codigo

        if len(tbls_bajo_corte) != 0 or len(tbls_sobre_corte) != 0:
            dic =dict(zip(['error','evidencia'],[False,tbls_bajo_corte.items()]))
        else:
            dic =dict(zip(['error','evidencia'],[True,'Ok']))

        return(dic)

    def check_errorcode(self,diccionario):
        """
        Función que identifica si query create insert tiene posteriormente errorcode asignado; y si es que el número del error code es único y ascendente respecto
        a queries previas
        Autor: Ramiro Burgos
        """
        tipo_query = diccionario['metadata']['tipo']

        if tipo_query in ['create','insert','collect']:

            indice_revision_errorcode = [x+1 for x,y in enumerate(self.arg) if y==diccionario] #check if create, insert have if_errorcode beneath code
            diccionario_check_error = self.arg[indice_revision_errorcode[0]]
            bool_check = 'if_errorcode'==diccionario_check_error['metadata']['tipo']
            if bool_check:
                exit_number = diccionario_check_error['metadata']['select']['exit_num'].strip().split()[-1]#si existe errorcode, revisamos si la enumeración es correcta
                reversed_index = [self.arg[x]['metadata']['select']['exit_num'].strip().split()[-1] for x in reversed(range(0,indice_revision_errorcode[0])) if self.arg[x]['metadata']['tipo']=='if_errorcode']
                bool_check_number = [not int(exit_number)>int(x) for x in reversed_index] # se da vuelta lógica para que incidencia sea True
                lista_evidencia_orden = list(compress(reversed_index,bool_check_number))
                dic =dict(zip(['error','evidencia'],[bool(lista_evidencia_orden),lista_evidencia_orden]))
            else:
                dic = dict(zip(['error', 'evidencia'], [True, diccionario_check_error['metadata']['tipo']]))
        else:
            dic = dict(zip(['error', 'evidencia'], [False, None]))
        return dic
